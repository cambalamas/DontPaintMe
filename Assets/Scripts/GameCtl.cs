﻿// ------------------------ //
//  "Dont Paint Me" - 2017  //
//    Daniel Camba Lamas    //
//  <cambalamas@gmail.com>  //
// ------------------------ //

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameCtl : MonoBehaviour {

	// ************************************************************************
	//	--- VARIABLES ---
	// ************************************************************************

	// From editor
	public Transform npc;
	public Transform enemy;
	public Transform player;
	public GameObject guihud;
	public GameObject guiwin;
	public GameObject guidraw;
	public GameObject guilose;
	public GameObject guipause;
	public GameObject guipauseButtonBACK;
	public GameObject guihudTime;
	public GameObject guihudRedPoints;
	public GameObject guihudBluePoints;
	public List<Material> skies;

	// Consts
	float timer;
	const int maxNPC = 25;
	const int npcValue = 1;
	const float initTimer = 60f;
	const float SpawnTime = 0.25f; // 0.75f - 0.2f

	// Backend
	float X;
	float Z;
	int npcs;
	bool gameOver;
	int redPoints;
	int bluePoints;
	bool onGameScene;
	bool allNpcGenerated;

	// GUI
	string TimeData = "0:00";

	// ************************************************************************
	//	--- UNITY BASE METHODS ---
	// ************************************************************************

	void Awake() {
		timer = initTimer;

		var ground = GameObject.FindWithTag("Terrain").transform.lossyScale;
		X = ground.x * 0.5f * 0.5f;
		Z = ground.z * 0.5f * 0.5f;

		onGameScene = SceneManager.GetActiveScene().buildIndex == 1;

		if (onGameScene) {

			// "Random" skybox
			var sky0 = PlayerPrefs.GetInt("Skybox"); // Catch value
			RenderSettings.skybox = skies[sky0];
			var nextSky = (sky0 + 1 < skies.Count) ? sky0 + 1 : 0;
			PlayerPrefs.SetInt("Skybox", nextSky); // Set next value

			// Setup GUI
			guihud.SetActive(true);
			guiwin.SetActive(false);
			guidraw.SetActive(false);
			guilose.SetActive(false);
			guipause.SetActive(false);
		}
	}

	void Start() {
		var enemyInitPos = new Vector3(0f, 0f, Z + 5.0f);
		var playerInitPos = new Vector3(0f, 0f, -Z - 5.0f);
		Instantiate(player, playerInitPos, Quaternion.identity);
		Instantiate(enemy, enemyInitPos, Quaternion.Euler(0f, 180f, 0f));
		StartCoroutine(NpcGenerator());
	}

	void Update() {
		if (onGameScene) {

			// Timer
			int minutes = Mathf.FloorToInt(timer / 60F);
			int seconds = Mathf.FloorToInt(timer % 60F);
			TimeData = string.Format("{0:0}:{1:00}", minutes, seconds);
			timer -= Time.deltaTime;

			// NPC counter
			npcs = GameObject.FindGameObjectsWithTag("NPC").Length;

			// Check game over
			if (timer <= 0) {
				GameOver();
			}

			// Re-Generate NPCs on board
			if (npcs <= 0 && allNpcGenerated) {
				StartCoroutine(NpcGenerator());
				allNpcGenerated = false;
			}

			// Pause menu
			if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown("joystick button 7")) {
				if (Time.timeScale == 1) {
					guihud.gameObject.SetActive(false);
					guipause.gameObject.SetActive(true);
					guipauseButtonBACK.GetComponent<Button>().Select();
					Time.timeScale = 0;
				} else {
					guipause.gameObject.SetActive(false);
					guihud.gameObject.SetActive(true);
					Time.timeScale = 1;
				}
			}

		}
	}

	void OnGUI() {
		if (onGameScene) {
			if (guihud.activeInHierarchy == true) {
				guihudTime.GetComponent<Text>().text = TimeData;
				guihudRedPoints.GetComponent<Text>().text = redPoints.ToString() + " Pts";
				guihudBluePoints.GetComponent<Text>().text = bluePoints.ToString() + " Pts";
			}
		}
	}

	// ************************************************************************
	//	--- HELPERS ---
	// ************************************************************************

	// Add point to the given PJ
	void AddPointsTo(string who) {
		if (who == "red") redPoints += npcValue;
		if (who == "blue") bluePoints += npcValue;
	}

	// Actions to do when the game is over
	void GameOver() {
		guihud.SetActive(false);
		var hsRed = PlayerPrefs.GetInt("HighScoreRED");
		var hsBlue = PlayerPrefs.GetInt("HighScoreBLUE");
		var winsRed = PlayerPrefs.GetInt("WinsRED");
		var winsBlue = PlayerPrefs.GetInt("WinsBLUE");
		// Check points to determine win case
		if (!gameOver) {
			// Win
			if (redPoints > bluePoints) {
				guiwin.SetActive(true);
				if (redPoints > hsRed) {
					PlayerPrefs.SetInt("HighScoreRED", redPoints);
				}
				PlayerPrefs.SetInt("WinsRED", winsRed + 1);
			}
			// Lose
			else if (bluePoints > redPoints) {
				guilose.SetActive(true);
				if (bluePoints > hsBlue) {
					PlayerPrefs.SetInt("HighScoreBLUE", bluePoints);
				}
				PlayerPrefs.SetInt("WinsBLUE", winsBlue + 1);
			}
			// Draw
			else {
				guidraw.SetActive(true);
			}
			gameOver = true;
		}
		// For back to main menu, press right click
		StartCoroutine(BackToMainMenuFromGameOver());
	}

	// Create NPCs
	IEnumerator NpcGenerator() {
		for (int i = 0; i < maxNPC; i++) {
			yield return new WaitForSeconds(SpawnTime);
			var pos = new Vector3(Random.Range(-X, X), 0f, Random.Range(-Z * 0.25f, Z * 0.25f));
			Instantiate(npc, pos, Quaternion.identity); // Inst the NPC
		}
		allNpcGenerated = true;
	}

	// Exit the game from the pause menu
	public void BackToMainMenuFromPause() {
		Time.timeScale = 1; // Un-Pause
		SceneManager.LoadScene(0);
	}

	// Wait a little time before back to the main menu after game over
	IEnumerator BackToMainMenuFromGameOver() {
		yield return new WaitForSeconds(3.5f);
		SceneManager.LoadScene(0);
	}

}
