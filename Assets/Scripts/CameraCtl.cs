﻿// ------------------------ //
//  "Dont Paint Me" - 2017  //
//    Daniel Camba Lamas    //
//  <cambalamas@gmail.com>  //
// ------------------------ //

using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Remoting.Contexts;
using UnityEngine;

public class CameraCtl : MonoBehaviour {

	// ************************************************************************
	//	--- VARIABLES ---
	// ************************************************************************

	// Serializable
	bool verticalInvert;

	// Backend
	Transform target;
	float xRot, yRot, zRot;
	Vector2 oLim = new Vector2(-10f, 10f);
	Vector2 vLim = new Vector2(-5f, 5f);
	Vector3 offset = new Vector3(0.5f, 2f, 0f);

	// ************************************************************************
	//	--- UNITY BASE METHODS ---
	// ************************************************************************

	void Awake() {
		verticalInvert = (PlayerPrefs.GetInt("Y-Axis") == 1) ? true : false;
	}

	void Update() {
		if (target != null) {
			// Move attached to player
			transform.position = target.position + offset;
			// Horizontal rotation
			float rotY = Input.GetAxisRaw("Mouse X");
			if (!rotY.Equals(0f)) {
				yRot += rotY * 2f;
				// zRot += rotY * 2f;
				// zRot = Mathf.Clamp(zRot, oLim[0], oLim[1]);
			}
			// Vertical rotation
			float rotX = Input.GetAxisRaw("Mouse Y");
			if (!rotX.Equals(0f)) {
				xRot += (verticalInvert) ? -rotX : rotX;
				xRot = Mathf.Clamp(xRot, vLim[0], vLim[1]);
			}
			// Apply rotation
			transform.rotation = Quaternion.Euler(xRot, yRot, zRot);
		} else { // Fix to init player as prefab
			var tgt = GameObject.FindWithTag("Player");
			if (tgt != null) { target = tgt.transform; }
		}
	}

}
